//
//  STKCachedQueueItemID.h
//  ODACloudMailRu
//
//  Created by den on 08/05/17.
//  Copyright © 2017 den. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface STKCachedQueueItemID : NSObject <NSCopying>

@property (copy, nonatomic, nonnull) NSString *cacheFilePath;
@property (strong, nonatomic, nonnull) NSURL *url;
@property (strong, nonatomic, nullable) NSDictionary *userInfo;
@property (assign, nonatomic) BOOL isCached;
@property (assign, nonatomic) BOOL isCancelled;

/// Item will be saved to file specified in filePath

- (instancetype)initWithURL: (NSURL * _Nonnull) url
              cacheFilePath: (NSString * _Nonnull) filePath
                   userInfo: (NSDictionary * _Nullable) userInfo;

@end
