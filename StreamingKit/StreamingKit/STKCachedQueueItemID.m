//
//  STKCachedQueueItemID.m
//  ODACloudMailRu
//
//  Created by den on 08/05/17.
//  Copyright © 2017 den. All rights reserved.
//

#import "STKCachedQueueItemID.h"

@implementation STKCachedQueueItemID

- (instancetype)initWithURL:(NSURL * _Nonnull) url
              cacheFilePath: (NSString * _Nonnull) filePath
                   userInfo: (NSDictionary * _Nullable) userInfo {
    self = [super init];
    if (self) {
        _url = url;
        _cacheFilePath = filePath;
        _isCached = NO;
        _isCancelled = NO;
        _userInfo = userInfo;
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone{
    STKCachedQueueItemID *otherObject = [[STKCachedQueueItemID alloc]init];
    otherObject->_cacheFilePath = [self.cacheFilePath copyWithZone:zone];
    otherObject->_url = [self.url copyWithZone:zone];
    otherObject->_isCached = self.isCached;
    otherObject->_isCancelled = self.isCancelled;
    otherObject->_userInfo = self.userInfo;
    return otherObject;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ [%@, %@]", self.cacheFilePath.lastPathComponent, self.url, self.userInfo];
}
@end
